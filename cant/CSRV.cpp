/*
 * YTCNSRV.cxx
 *
 * Copyright 2021  <pi@master>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef CSRV_H
#define CSRV_H

#include <iostream>

#include <algorithm>
#include <fcntl.h>
#include <netinet/in.h>
#include <set>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <net/if.h>
#include <string.h>
#include <sys/ioctl.h>
#include <time.h>

#include <fstream>
#include <iostream>

#include <bitset>

#include "cans.h"
#include "cant.h"

using namespace std;

#define SERVERPORT 3425

#define TIMESHIFT 946684800 // 01.01.2000 00:00

#define THIS_NODE_BUS_ID 11
#define BROADCAST_NODE_BUS_ID 31

// const std::vector<std::string> StrName = {"Type", "Sender", "Sender_NET",
// "Reciever", "Reciever_NET", "ID_CRC8", "Flags", "Repeat"}; const
// std::vector<uint8_t> StrLen = {1, 5, 2, 5, 2, 8, 5, 1};

const std::vector<std::string> StrName
    = { "Repeat",   "Flags",      "ID_CRC8", "Reciever_NET",
        "Reciever", "Sender_NET", "Sender",  "Type" };
const std::vector<uint8_t> StrLen = { 1, 5, 8, 2, 5, 2, 5, 1 };

// выдается временной маркер (число милисекунд от события)
uint32_t GETTICK()
{

    struct timespec curtm;

    clock_gettime(CLOCK_REALTIME, &curtm);
    // cout<<"Get Tick!!! "<<" nsec "<<curtm.tv_nsec<<endl;
    return (uint32_t)curtm.tv_nsec;

}; // (*getTick)();

/*
uint8_t PrintFrames(cant *frame)
{
    printf("Adr %d ", frame->getAdr());
    printf("s:%d ", frame->getAttrByName("Sender"));
    printf("r:%d ", frame->getAttrByName("Receiver"));
    printf("a:%d ", frame->getAttrByName("Attribute"));
    printf("f:%d ", frame->getAttrByName("Flags"));

    for (int k = 0; k < frame->frm.can_dlc; k++)
        printf("%d ", frame->frm.data[k]);

    printf("\n");

    return 0;
};
*/

uint8_t receiveFRAME(cant *frame)
{

    printf("rec. frame:\n");
    frame->PrintFrame();

    return 0;
};

int main(int argc, char **argv)
{

    cout << "Start Simple CAN-TCP NonBlock Server!" << endl;

    cout << "Parameters: ";
    cout << argc << " : ";
    for (int i = 0; i < argc; i++)
        {
            cout << argv[i] << " | ";
        };
    cout << endl;

    string aa = " is my NodeID (Sender NodeID)";

    int MyID = 0;
    int MyNetID = 0;

    int RunMode = 0; // режим работы. по умолчанию 0 - передатчик
    // 1 - приемник

    if (argc == 1)
        {
            cout << "Start without parameters! ";
            MyID = THIS_NODE_BUS_ID;
        };

    if (argc == 2)
        {
            // string sss;
            // sss=argv[1];
            cout << "Start with 1 parameter! "
                 << endl; // argv[1]<<" sss="<<sss<<endl;

            // MyID=std::stoi(sss);
            MyID = stoi(argv[1]);
        };

    if (argc == 3)
        {
            cout << "Start with 2 parameter! " << endl;
            MyID = stoi(argv[1]);
            RunMode = stoi(argv[2]);
        };
    if (argc > 3)
        {

            cout << "Unable to start with more than 2 parameters! ";

            exit(999);
        };

    // CAN +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    cans SocCAN;          // объект сокета
    SocCAN.start("can0"); // старт сокета

    if (RunMode == 0)
        {
            cout << "Sender Mode. RunMode=" << RunMode << endl;
            // ТЕСТИРУЕМ СВОЙ ОБЪЕКТ CAN

            printf(" cant FR start!!!\n");
            cant FR(StrName, StrLen);

            printf(" FR.dlc=%d\n", FR.dlc);

            FR.setAttrByName("Sender", 1);
            FR.setAttrByName("Reciever", 3);
            FR.setAttrByName("Flags", 31);
            // FR.setAttrByName("Attribute", 7);

            FR.codeId();

            cout << "Adr: " << endl;

            bitset<32> x(FR.getAdr());
            bitset<32> y(FR.getAdr());
            bitset<32> b(CAN_EFF_FLAG);

            cout << " CAN_EFF_FLAG = " << b << '\n';
            cout << "bit CAN    ID = " << x << '\n';
            cout << "bit CAN EFFID = " << y << '\n';

            FR.decodeId();

            bitset<32> z(FR.getAdr());
            bitset<32> z1(FR.frm.can_id);

            cout << "bit CAN   .ID = " << z << '\n';
            cout << "bit CANfrm.ID = " << z1 << '\n';

            int f = FR.getAttrByName("Flags");
            cout << "Flags=" << f << endl;

            printf("id=%d \n", FR.getAdr());

            // for(int i = 0;i<FR.dlc;i++)
            //            FR.frm.data[i]=65;//FR.data[i];

            char AA[] = "BBBBCCCC";

            FR.input8char(AA);

            int bytes_send = 0;
            for (int jj = 0; jj < 10; jj++)
                {
                    // bytes_send = send(SocCAN.socan, FR.getFrame(),
                    // sizeof(struct can_frame), MSG_DONTWAIT);
                    bytes_send = SocCAN.sendFrame(FR);

                    if (bytes_send < 0)
                        {
                            // myfile<<"ERROR CANSEND SERVER!!! "<<jj<<endl;
                            sleep(1);
                        }
                    else
                        {
                            jj = 100;
                        };
                }; // for

        }; // if RunMode==0

    if (RunMode == 1)
        {
            cout << "Receiver Mode. RunMode=" << RunMode << endl;

            cant FR(StrName, StrLen);

            for (int i = 0; i < FR.IdAttrName.size(); i++)
                {

                    bitset<32> mm(FR.IdAttrMask[i]);
                    cout << " IdAttrMask[" << i << "] = " << mm
                         << " IdAttrShift[" << i
                         << "] = " << (int)FR.IdAttrShift[i] << " IdAttrName["
                         << i << "] = " << FR.IdAttrName[i] << '\n';
                }

            // FR.setAttrByName("Sender", 1);
            // FR.setAttrByName("Receiver", 3);
            // FR.setAttrByName("Flags", 31);
            // FR.setAttrByName("Attribute", 7);

            int N = FR.getIdAttrSize();
            for (int i = 0; i < N; i++)
                // FR.IdAttrValue[i] = 1 + i * 2;
                FR.setAttrByNdx(i, 1 + i * 2);

            FR.codeId();
            bitset<32> mmm(FR.frm.can_id);
            cout << " :  = " << mmm << " Id= " << (int)FR.frm.can_id << '\n';

            for (int i = 0; i < FR.IdAttrName.size(); i++)
                {
                    FR.IdAttrValue[i] = 111;
                }

            FR.decodeId();
            for (int i = 0; i < FR.IdAttrName.size(); i++)
                {
                    bitset<32> mm(FR.IdAttrValue[i]);
                    // bitset<32> mmm(FR.IdAttrMask[i]);
                    // bitset<32> UUU(FR.Id);
                    // cout << " mm=" << mm << " IdAttrValue[" << i << "] = "
                    // << (int)FR.IdAttrValue[i] << '\n';
                    cout << " mm=" << mm << " IdAttrValue[" << i
                         << "] = " << (int)FR.getAttrByNdx(i) << '\n';
                    // cout << " UUU=" << UUU << '\n';
                    // cout << " mmm=" << mmm << '\n';

                    // bitset<32> aa(FR.Id & FR.IdAttrMask[i]);
                    // cout << " aaa=" << aa << '\n';
                }

            cout << "Receiver=" << (int)FR.getAttrByName("Reciever") << '\n';

            printf(" FR.dlc=%d\n", FR.dlc);

            // FR.setSender(1);
            // FR.setReceiver(3);
            // FR.setFlags(31);
            // FR.setAttribute(7);

            bitset<32> z(FR.getAdr());
            bitset<32> z1(FR.frm.can_id);

            cout << "bit CAN   .ID = " << z << '\n';
            cout << "bit CANfrm.ID = " << z1 << '\n';

            int f = FR.getAttrByName("Flags");
            cout << "Flags=" << f << endl;

            printf("id=%d \n", FR.getAdr());

            char AA[] = "XXXXXXXX";

            FR.input8char(AA);
            cout << "Befor receive " << endl;

            FR.PrintFrame();

            int REZ = 0;

            REZ = SocCAN.recvFrame(&FR, 1, 100);

            if (REZ < 0)
                {
                    cout << "recv ERROR!!!" << endl;
                };

            if (REZ > 0)
                {
                    cout << "After receive " << endl;
                    FR.PrintFrame();
                }; //

            if (REZ == 0)
                cout << "Time out!!! " << endl;

        }; //

    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////

    // Закрываем сокет в конце штатной работы

    SocCAN.stop();

    return 0;
} // int main()

#endif // CSRV_H
