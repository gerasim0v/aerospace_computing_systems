1. В функциях должны быть указаны типы аргументов и возвращаемых значений. Т.е. вместо
```python
def my_func(a, b):
    return a + b
```
Должно быть, например (если передаём вещественные значения):
```python
def my_func(a: float, b: float) -> float:
    return a + b
```

2. У функций должны быть добавлены doc-string, для этого удобно использовать расширение
VSCode
[autodoctring](https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring),
которое делает большую часть работы. В этом проекте данное расширение добавлено в набор
по умолчанию. См. гиф анимацию по работе расширения:

<img src="https://github.com/NilsJPWerner/autoDocstring/raw/HEAD/images/demo.gif"
alt="autodoctring" width="600"/>

3. Строки комментариев должны укладываться в длину строки 88 символов (первая
вертикальная линия). Удобно использовать добавленное в умолчания для проекта расширение
[rewrap](https://marketplace.visualstudio.com/items?itemName=stkb.rewrap)

Т.е. вместо:
```python
def my_func(a: float, b: float) -> float:
    """Вычисляет много всего. Делает это замечательно, великолепно. Только документация большая, коротко не описать без потери смысла."""
```

должно быть:
```python
def my_func(a: float, b: float) -> float:
    """Вычисляет много всего. Делает это замечательно, великолепно. Только документация
    большая, коротко не описать без потери смысла.
    """
```

4. Код должен быть отформатирован с помощью Black. Если работаете в VSCode, то настройка
автоматически форматировать файл при сохранении добавлено в файл .vscode/settings.json.
Необходимо использовать плагин
[Black](https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter)
для VSCode

5. Линтер Ruff не должен выдавать ошибок, который доступен в виде
[плагина](https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff)

Указанные выше плагины добавлены в файл .vscode/recommendations.json и при первом
запуске будут предложены к установке.
