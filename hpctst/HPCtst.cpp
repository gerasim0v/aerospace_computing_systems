
#if !defined(__linux__)
#include "stdafx.h"
#endif

#include <fstream>
#include <cstdlib>

#include "mpi.h"
#include <math.h>
#include "stdio.h"
#include "stdlib.h"

#include "string.h" //cstring

#include "unita.h"  //Конвертация целого числа в строку char*

using namespace std;


//Начало и конец счета (в шагах)
int start=0;
int stop=0;

//Размерности массивов
const int max_vorton=20000;
const int max_procs=130;
long double eps=0.10;
long double eps2=eps*eps;


const long double dpi=6.283185307179586476925286766559005768394;
const long double pi=0.5*dpi;
const long double quadpi = 2.0*dpi;
const long double octpi = 4.0*dpi;

const long double iquadpi = 1.0/quadpi;
const long double ioktpi =  1.0/octpi;



const long double ajm[3][5]= {{0.0, 0.0, 0.0, 0.0, 0.0},
                     {-0.774596669241483, 0.0, 0.774596669241483, 0.0, 0.0},
					 {-0.906179845938664, -0.538469310105683, 0.0, 0.538469310105683, 0.906179845938664}};

const long double hjm[3][5]= {{2.0, 0.0, 0.0, 0.0, 0.0},
					 {0.555555555555556, 0.888888888888889, 0.555555555555556, 0.0, 0.0},
                     {0.236926885056189, 0.478628670499366, 0.568888888888889, 0.478628670499366, 0.236926885056189}};

const int ngpm[3]= {1,3,5}; //Число гауссовых точек



int nBUB=0; //Количество бубликов
int nVTN=0; //Количество вортонов в каждом бублике
int mnogVTN=1;


int vtn_ndx=0; //Номер пробного вортона (для вывода телеметрии)

int shema=0; //1-прямая, 2-трансп., 3-симм.

int usertypeg=0; //0 - 1 г.т., 1 - 3 г.т., 2 - 5 г.т. (г.т. = гауссовы точки)

typedef struct
{
 long double r[3];
 long double dr[3];
 long double G;
} Vorton;


typedef struct
{
 long double pr[3];
 long double pdr[3];
} PVorton;

MPI_Datatype mpiVorton, mpiPVorton;




//void Induct(int tn, int vt, long double *VN, long double *VNx, long double *VNy, long double *VNz);



Vorton  POS[max_vorton]; //Сами вортоны
PVorton VEL[max_vorton]; //Приращения r и dr от шага к шагу


Vorton  POS_old[max_vorton]; //Сами вортоны


long double OMEGA[3];
long double Av[3];
long double Iv[3];


int n=0;    //Количество вортонов в пелене
long double dt=1.0; //Шаг
long double startwtime, endwtime; //Засечки времени
int cnt; // Счетчик промежут. шагов
int deltacnt = 1;  //каждый 1-ый (по умолчанию)

long double dl_min_etalon = 0.0;
long double dl_max_etalon = 100.0;


int myid, numprocs; //Системные
int namelen;
char processor_name[MPI_MAX_PROCESSOR_NAME];

//Управление распараллеливанием
int session_start[max_procs];
int session_length[max_procs];
long double session_spd[max_procs];
long double session_time[max_procs];
long double session_spd_sum;



ofstream logfile;
ofstream telefile;
ofstream timefile;

ofstream spdfile;
ofstream prsfile;


long double tmm[10]; //Времена



void save_to_file(int _step)
{

	char *fname1;
	fname1 = "Kadr";

	char *fname2;
	fname2 = ".txt";

    char *fzero;
	fzero = "0";

	char fstep[6];
	char fname[15];
    //char fstepx[6];

	fname[0] = '\0';

    //itoax(_step,fstep,fstepx,10);
	itoaxx(_step,fstep,10);

	strcat(fname,fname1);

	if (_step<10) strcat(fname,fzero);
	if (_step<100) strcat(fname,fzero);
	if (_step<1000) strcat(fname,fzero);
	if (_step<10000) strcat(fname,fzero);

	strcat(fname,fstep);
	strcat(fname,fname2);

	ofstream outfile;
	outfile.open(fname);

	outfile << n << endl; //Сохранение числа вихрей в пелене

	for (int i=0; i<n; i++)
		{
		outfile << (int)(i) << endl;
		outfile << (long double)(POS[i].G)  << endl;
		outfile << (long double)(POS[i].r[0])   << " " << (long double)(POS[i].r[1])   << " " << (long double)(POS[i].r[2])   << endl;
        outfile << (long double)(POS[i].dr[0])  << " " << (long double)(POS[i].dr[1])  << " " << (long double)(POS[i].dr[2])  << endl;
		}
	outfile.close();
} //save_to_file


void save_dkadr_to_file(int _step)
{
	char *fname1;
	fname1 = "DKadr";

	char *fname2;
	fname2 = ".txt";

    char *fzero;
	fzero = "0";

	char fstep[6];
	char fname[16];
    //char fstepx[6];

	fname[0] = '\0';

    //itoax(_step,fstep,fstepx,10);
	itoaxx(_step,fstep,10);

	strcat(fname,fname1);

	if (_step<10) strcat(fname,fzero);
	if (_step<100) strcat(fname,fzero);
	if (_step<1000) strcat(fname,fzero);
	if (_step<10000) strcat(fname,fzero);

	strcat(fname,fstep);
	strcat(fname,fname2);

	ofstream outfile;
	outfile.open(fname);

	outfile << n << endl; //Сохранение числа вихрей в пелене

	for (int i=0; i<n; i++)
		{
		outfile << (int)(i) << " ";
		outfile << (long double)(POS[i].G)  << " ";
		outfile << (long double)(POS[i].dr[0])   << " " << (long double)(POS[i].dr[1])   << " " << (long double)(POS[i].dr[2])  << " ";
		outfile << (long double)(VEL[i].pdr[0])  << " " << (long double)(VEL[i].pdr[1])  << " " << (long double)(VEL[i].pdr[2])  << endl;
		}



	outfile.close();
} //save_to_file


void save_vkadr_to_file(int _step)
{
	char* fname1;
	fname1 = "VKadr";

	char* fname2;
	fname2 = ".txt";

	char* fzero;
	fzero = "0";

	char fstep[6];
	char fname[16];
	//char fstepx[6];

	fname[0] = '\0';

	//itoax(_step,fstep,fstepx,10);
	itoaxx(_step, fstep, 10);

	strcat(fname, fname1);

	if (_step < 10) strcat(fname, fzero);
	if (_step < 100) strcat(fname, fzero);
	if (_step < 1000) strcat(fname, fzero);
	if (_step < 10000) strcat(fname, fzero);

	strcat(fname, fstep);
	strcat(fname, fname2);

	ofstream outfile;
	outfile.open(fname);

	outfile << n << endl; //Сохранение числа вихрей в пелене

	for (int i = 0; i < n; i++)
	{
		outfile << (int)(i) << " ";
		outfile << (long double)(POS[i].G) << " ";
		outfile << (long double)(VEL[i].pr[0] / dt) << " " << (long double)(VEL[i].pr[1] / dt) << " " << (long double)(VEL[i].pr[2] / dt) << " ";
		outfile << (long double)(VEL[i].pdr[0] / dt) << " " << (long double)(VEL[i].pdr[1] / dt) << " " << (long double)(VEL[i].pdr[2] / dt) << endl;

	}



	outfile.close();
} //save_vkadr_to_file



//Сохранение вихревого следа в файл .vtk
void SaveKadrVtk(int _step)
{
	char* fname1;
	fname1 = "VTKadr";

	char* fname2;
	fname2 = ".vtk";

	char* fzero;
	fzero = "0";

	char fstep[6];
	char fname[16];
	//char fstepx[6];

	fname[0] = '\0';

	//itoax(_step,fstep,fstepx,10);
	itoaxx(_step, fstep, 10);

	strcat(fname, fname1);

	if (_step < 10) strcat(fname, fzero);
	if (_step < 100) strcat(fname, fzero);
	if (_step < 1000) strcat(fname, fzero);
	if (_step < 10000) strcat(fname, fzero);

	strcat(fname, fstep);
	strcat(fname, fname2);

	std::ofstream outfile;

	auto cut = [](double x) -> double
	{
		return (fabs(x) < 1e-16) ? 0.0 : x;
	};

	double nodes[2*max_vorton][3];

	int ndx = 0;
	for (size_t k = 0; k < n; k++)
	{
		for (size_t kk = 0; kk < 3; kk++) nodes[ndx][kk] = POS[k].r[kk] + POS[k].dr[kk];
		ndx++;
		for (size_t kk = 0; kk < 3; kk++) nodes[ndx][kk] = POS[k].r[kk] - POS[k].dr[kk];
		ndx++;
	}

	outfile.open(fname);

	outfile.precision(15);

	outfile << "# vtk DataFile Version 2.0" << std::endl;
	outfile << "VM3D VTK result: " << fname << " saved 30 August 2021 at 22:21:01" << std::endl;
	outfile << "ASCII" << std::endl;
	outfile << "DATASET POLYDATA" << std::endl;
	outfile << "POINTS " << 2*n << " float" << std::endl;
	//for (size_t k = 0; k < n; k++)
	//	outfile << cut(POS[k].r[0]) << " " << cut(POS[k].r[1]) << " " << cut(POS[k].r[2]) << std::endl;

	for (size_t k = 0; k < (2*n); k++)
		outfile << cut(nodes[k][0]) << " " << cut(nodes[k][1]) << " " << cut(nodes[k][2]) << std::endl;

	//*
	outfile << "LINES " << n << " " << n * 3 << std::endl;
	//outfile << "LINES " << pet.size() << " " << pos.size() + pet.size() * 2 << std::endl;
	size_t s = 0;
	for (size_t i = 0; i < n; i++)
	{

		outfile << 2;
		outfile << " " << s;
		s++;
		outfile << " " << s<< std::endl;
		s++;
	}
	//*/
	outfile.close();


}//SaveKadrVtk(...)



inline long double L2(long double *X){return X[0]*X[0]+X[1]*X[1]+X[2]*X[2];}
inline long double dot(long double *X, long double *Y){return X[0]*Y[0]+X[1]*Y[1]+X[2]*Y[2];}
inline long double cub(long double x) {return x*x*x;}
inline long double sqr(long double x) {return x*x;}

//7. Векторное произведение С=A x B
inline void Cross(long double *A, long double *B, long double *C){
C[0]=A[1]*B[2]-A[2]*B[1];
C[1]=A[2]*B[0]-A[0]*B[2];
C[2]=A[0]*B[1]-A[1]*B[0];
};//Cross







//Получение вектора Rez длиной L в направлении вектора Vec
inline long double DirVec(long double *Vec, long double *Rez, long double L){
	long double M=sqrtl(L2(Vec));
	long double mn=L/M;
	Rez[0]=mn*Vec[0]; Rez[1]=mn*Vec[1]; Rez[2]=mn*Vec[2];
	return M;
}//DirVec









inline void dVmdRn1(long double gam, long double c, long double *a, long double *h, long double *s0, long double *s1, long double *s2, long double ms1, long double ms2, long double a2, long double *B)
{
	long double dobc=2.0*c;

	long double mnog1 = 1.0/ms2 - 1.0/ms1 + dobc*dot(s0,h);

	long double mnog2[3];

	long double icubms1=1/cub(ms1);
	long double icubms2=1/cub(ms2);

	long double gamiquadpi=gam*iquadpi;

	long double dCdRn[3];


	//n=0 k=0,1,2
	mnog2[0] = s2[0]*icubms2*s2[0] - s1[0]*icubms1*s1[0] + dobc*s0[0]*h[0];
	mnog2[1] = s2[0]*icubms2*s2[1] - s1[0]*icubms1*s1[1] + dobc*s0[0]*h[1];
	mnog2[2] = s2[0]*icubms2*s2[2] - s1[0]*icubms1*s1[2] + dobc*s0[0]*h[2];
	dCdRn[0] = (mnog1*h[0] - dot(mnog2,h))/a2;


	//n=1 k=0,1,2
	mnog2[0] = s2[1]*icubms2*s2[0] - s1[1]*icubms1*s1[0] + dobc*s0[1]*h[0];
	mnog2[1] = s2[1]*icubms2*s2[1] - s1[1]*icubms1*s1[1] + dobc*s0[1]*h[1];
	mnog2[2] = s2[1]*icubms2*s2[2] - s1[1]*icubms1*s1[2] + dobc*s0[1]*h[2];
    dCdRn[1] = (mnog1*h[1] - dot(mnog2,h))/a2;


	//n=2 k=0,1,2
	mnog2[0] = s2[2]*icubms2*s2[0] - s1[2]*icubms1*s1[0] + dobc*s0[2]*h[0];
	mnog2[1] = s2[2]*icubms2*s2[1] - s1[2]*icubms1*s1[1] + dobc*s0[2]*h[1];
	mnog2[2] = s2[2]*icubms2*s2[2] - s1[2]*icubms1*s1[2] + dobc*s0[2]*h[2];
    dCdRn[2] = (mnog1*h[2] - dot(mnog2,h))/a2;

	//n=0, m=0
	B[0] = gamiquadpi*(dCdRn[0]*a[0]);
    //n=1, m=0
	B[1] = gamiquadpi*(dCdRn[1]*a[0] - h[2]);
	//n=2, m=0
	B[2] = gamiquadpi*(dCdRn[2]*a[0] + h[1]);

	//n=0, m=1
	B[3] = gamiquadpi*(dCdRn[0]*a[1] + h[2]);
    //n=1, m=1
	B[4] = gamiquadpi*(dCdRn[1]*a[1]);
	//n=2, m=1
	B[5] = gamiquadpi*(dCdRn[2]*a[1] - h[0]);

	//n=0, m=2
	B[6] = gamiquadpi*(dCdRn[0]*a[2] - h[1]);
    //n=1, m=2
	B[7] = gamiquadpi*(dCdRn[1]*a[2] + h[0]);
	//n=2, m=2
	B[8] = gamiquadpi*(dCdRn[2]*a[2]);
}




void Induct2011(int tn, int vt, long double *VNres, long double *VNxres, long double *VNyres, long double *VNzres)
{
  long double s0[3];
  long double s1[3];
  long double s2[3];

  long double h[3];

  long double os1[3];
  long double os2[3];

  long double ms1,ms2;

  long double dort[3];

  long double a[3];

  long double d2, d;

  long double epsX;
  long double epsX2;

  epsX2=eps2;
  epsX =eps;


  int typeg = 0;

  long double htn[3]={0.0,0.0,0.0};

  //if (tens)
  //{
	  typeg=usertypeg;
	  for (int i=0; i<3; i++)
	   htn[i]=POS[tn].dr[i];
  //} //if tens

  //else typeg=0;

  int ngp=ngpm[typeg];




  long double VN[3];
  long double BN[9];
  long double BNres[9];

  for (int i=0; i<3; i++)
   VNres[i]=0.0;

  //if (tens)
  //{
	for (int k=0; k<9; k++)
	{
		BNres[k]=0.0;
		BN[k]=0.0;
	}
  //}

  //Подготовительные операции
  long double VP[3]; //координаты точки наблюдения

  //Цикл по Гауссовым точкам
  for (int gp=0; gp<ngp; gp++)
  {

	//{
	for (int i=0;i<3;i++)
	  //if (tens)
		VP[i]=POS[tn].r[i]+ajm[typeg][gp]*htn[i];
	  //else
		//VP[i]=Rtn[i];
	//}

	//{
	for (int q=0;q<3;q++)
	{
	  h[q]=POS[vt].dr[q];
	  s0[q]=VP[q]-POS[vt].r[q];
	  s1[q]=s0[q]-h[q];
	  s2[q]=s0[q]+h[q];
	}//for q
	//}

	Cross(h,s0,a); //Здесь нашли a = h (x) s0

	long double a2=L2(a);
	long double h2=L2(h);

	/*
	if (h2>max_L2)
	{
		printf("L2 is too big!!!");
		logfile << "L2 is too big!!!";
		exit(1);
	}
	*/

	d2=a2/h2;//Квадрат расстояния до оси вортона

	if (d2>=epsX2)
	//влияние на далекую точку, закон Био-Савара
	{
		ms1=DirVec(s1,os1,1.0);
		ms2=DirVec(s2,os2,1.0);

		//{
		for (int i=0;i<3;i++)
		{
			dort[i]=os2[i]-os1[i];
		}//for i
		//}

		long double c=dot(dort,h)/a2;

		//Вычисляем скорость, инцируемую вортоном vt на вортон tn
		//{
		for (int q=0; q<3; q++)
		//{
			VN[q]=POS[vt].G * iquadpi * c * a[q];
		//}//for i
		//}

		//Определяем тензор деформации вортона tn под влиянием вортона vt
		//if (tens)
			dVmdRn1(POS[vt].G,c,a,h,s0,s1,s2,ms1,ms2,a2,BN);


	}//влияние на далекую точку, закон Био-Савара

	else
	{
		if (d2<1e-10)
		{
			for (int i=0; i<3; i++)
			VN[i]=0.0;

			//if (tens)
				for (int k=0; k<9; k++)
					BN[k]=0.0;
		}//if d2<1e-10


		else
		{
			//влияние на близкую точку, линейное сглаживание
			//1. Определение вектора tn

			d=sqrtl(d2);

			long double ialpha=epsX/d;
			long double delta[3];

			long double scal=dot(s0,h)/h2;
			{
				for (int i=0; i<3; i++)
					delta[i]=s0[i]-h[i]*scal;
			}

			//2.Фиктивная точка наблюдения
			long double r0a[3];
			{
				for (int i=0; i<3; i++)
	   				r0a[i]=VP[i] + delta[i]*(ialpha - 1.0);
			}

			//3.Вычисление влияния на фиктивную точку наблюдения r0a

			//Подготовительные операции
			{
			for (int i=0;i<3;i++)
			{
				//h[i]=POS[vt].h[i];
				s0[i]=r0a[i]-POS[vt].r[i];
				s1[i]=s0[i]-h[i];
				s2[i]=s0[i]+h[i];
			}//for i
			}

			Cross(h,s0,a); //Здесь нашли a = h (x) s0

			long double a2=L2(a);
			//long double h2=L2(h);

			ms1=DirVec(s1,os1,1.0);
			ms2=DirVec(s2,os2,1.0);

			{
			for (int i=0;i<3;i++)
			{
				dort[i]=os2[i]-os1[i];
			}//for i
			}

			long double c=dot(dort,h)/a2;

			{
			for (int i=0; i<3; i++)
			{
				VN[i]=POS[vt].G * iquadpi * c * a[i];
			}//for i
			}

			//Определяем тензор деформации вортона tn под влиянием вортона vt
			//if (tens)
				dVmdRn1(POS[vt].G,c,a,h,s0,s1,s2,ms1,ms2,a2,BN);

			//4. Сглаживание
			{

				//ialpha=1.0;  //  <<---  ?????????????????????????????????????????????????????????????????????????
				//ialpha=999999999999999999999.0;  //  <<---  ?????????????????????????????????????????????????????????????????????????

				for (int i=0; i<3; i++)
					VN[i]/=ialpha;

				//if (tens)
					for (int k=0; k<9; k++)
						BN[k]/=ialpha;
			}

		}//else if d2<1e-10


	}//влияние на близкую точку, линейное сглаживание

	for (int i=0; i<3; i++)
		VNres[i]+=VN[i]*hjm[typeg][gp]*0.5;


//	if (tens)
		for (int k=0; k<9; k++)
			BNres[k]+=BN[k]*hjm[typeg][gp]*0.5;



}//for gp

VNxres[0]=BNres[0];
VNyres[0]=BNres[1];
VNzres[0]=BNres[2];

VNxres[1]=BNres[3];
VNyres[1]=BNres[4];
VNzres[1]=BNres[5];

VNxres[2]=BNres[6];
VNyres[2]=BNres[7];
VNzres[2]=BNres[8];

} //void::InductGauss;






//-----------------------------------------------------------------------------------------------








//1. вихревое влияние на точку наблюдения
	//tn - номер вортона, на который считается влияние, индекс в массивах POS, VEL
	//vt - номер вортона, от которого считается влияние, индекс в массиве POS
    //VN - индуцированная скорость вортона
    //WN - индуцированная скорость удлиннения вортона
	//результат: VN, WN
void InductPoint(long double *Rtn, int vt, long double *VN)
{
  long double t0[3];
  long double t1[3];
  long double t2[3];

  long double dr[3];

  long double ot1[3];
  long double ot2[3];

  long double dort[3];

  long double R1,R2,R12,R22,Rp,Rm;

  long double a[3];

  long double d2, d;

  //Подготовительные операции
  {
  for (int i=0;i<3;i++)
  {
	  dr[i]=POS[vt].dr[i];
	  t0[i]=POS[vt].r[i]-Rtn[i];
	  t1[i]=t0[i]+dr[i];
	  t2[i]=t0[i]-dr[i];
  }//for i
  }

  Cross(t2,dr,a); //Здесь нашли 0.5*a

  {
  for (int i=0;i<3;i++)
   a[i]*=2.0;      //А теперь - целиком a
  }

  long double hh=L2(a);
  long double dr2=L2(dr);


  d2=0.25*hh/dr2;//Квадрат расстояния до оси вортона


  if(d2>=eps2)
 //влияние на далекую точку, закон Био-Савара
 {
  R12=L2(t1); R1=sqrtl(R12);
  R22=L2(t2); R2=sqrtl(R22);

  Rp=R1+R2;   Rm=R1-R2;


  {
  for (int i=0;i<3;i++)
  {
	  ot1[i]=t1[i]/R1; //орты направлений из центра вортона-источника влияния на концы вортона-точки наблюдения
	  ot2[i]=t2[i]/R2;
	  dort[i]=ot1[i]-ot2[i];
  }//for i
  }



  long double c = 2.0 * dot(dort,dr) / hh;

  //Вычисляем скорость, инцируемую вортоном vt на вортон tn
  {
  for (int i=0; i<3; i++)
  {
	  VN[i]=POS[vt].G * iquadpi * c * a[i];

  }//for i
  }


 }//влияние на далекую точку, закон Био-Савара
 else
 {//влияние на близкую точку, линейное сглаживание
  //1. Определение вектора tn

  d=sqrtl(d2);

  long double cft=eps/d;

  long double rtn[3];
  long double scal=dot(t0,dr)/dr2;
  {
  for (int i=0; i<3; i++)
   rtn[i]=-(t0[i]-dr[i]*scal);
  }

  //2.Фиктивная точка наблюдения
  long double r0a[3];
  {
  for (int i=0; i<3; i++)
   r0a[i]=Rtn[i] - rtn[i] + rtn[i]*cft;
  }

  //3.Вычисление влияния на фиктивную точку наблюдения r0a

  //Подготовительные операции
  {
  for (int i=0;i<3;i++)
  {
	  //dr[i]=POS[vt].dr[i];
	  t0[i]=POS[vt].r[i]-r0a[i];
	  t1[i]=t0[i]+dr[i];
	  t2[i]=t0[i]-dr[i];
  }//for i
  }

  Cross(t2,dr,a); //Здесь нашли 0.5*a

  {
  for (int i=0;i<3;i++)
   a[i]*=2.0;      //А теперь - целиком a
  }

  long double hh=L2(a);
  //long double dr2=L2(dr);


  //d2=0.25*hh/dr2;//Квадрат расстояния до оси вортона


  R12=L2(t1); R1=sqrtl(R12);
  R22=L2(t2); R2=sqrtl(R22);

  Rp=R1+R2;   Rm=R1-R2;


  {
  for (int i=0;i<3;i++)
  {
	  ot1[i]=t1[i]/R1; //орты направлений из центра вортона-источника влияния на концы вортона-точки наблюдения
	  ot2[i]=t2[i]/R2;
	  dort[i]=ot1[i]-ot2[i];
  }//for i
  }



  long double c = 2.0 * dot(dort,dr) / hh;

  //Вычисляем скорость, инцируемую вортоном vt на вортон tn
  {
  for (int i=0; i<3; i++)
  {
	  VN[i]=POS[vt].G * iquadpi * c * a[i];

  }//for i
  }



  //4. Сглаживание
  {
  for (int i=0; i<3; i++)
  {
   VN[i]/=cft;
  }//for i
  }


 }//влияние на близкую точку, линейное сглаживание

} //void::InductPoint;










//Влияние на ii-ый вортон
// ii - номер вортона, на который вычисляется влияние
// Результат - заполнение W, pW
void velo(int ii, long double* W, long double* pW)
{
{
  for (int i=0; i<3; i++)
  {
	W[i]=0.0;
	pW[i]=0.0;
  }//for i
}


long double VN[3];
long double VNx[3];
long double VNy[3];
long double VNz[3];

for (int k=0; k<n; k++)
{

if (k!=ii)
{
  Induct2011(ii,k,VN,VNx,VNy,VNz);

  W[0]+=VN[0];
  W[1]+=VN[1];
  W[2]+=VN[2];

  switch (shema)
  {
  //if (shema==1) //прямая
  case 1:
	  pW[0]+=VNx[0]*POS[ii].dr[0]+VNy[0]*POS[ii].dr[1]+VNz[0]*POS[ii].dr[2];
	  pW[1]+=VNx[1]*POS[ii].dr[0]+VNy[1]*POS[ii].dr[1]+VNz[1]*POS[ii].dr[2];
      pW[2]+=VNx[2]*POS[ii].dr[0]+VNy[2]*POS[ii].dr[1]+VNz[2]*POS[ii].dr[2];
	  break;

  //if (shema==2) //транспонированная
  case 2:
	  pW[0]+=VNx[0]*POS[ii].dr[0]+VNx[1]*POS[ii].dr[1]+VNx[2]*POS[ii].dr[2];
	  pW[1]+=VNy[0]*POS[ii].dr[0]+VNy[1]*POS[ii].dr[1]+VNy[2]*POS[ii].dr[2];
      pW[2]+=VNz[0]*POS[ii].dr[0]+VNz[1]*POS[ii].dr[1]+VNz[2]*POS[ii].dr[2];
	  break;

  //if (shema==3) //симметричная
  case 3:
  	  pW[0]+=            VNx[0] *POS[ii].dr[0]+0.5*(VNy[0]+VNx[1])*POS[ii].dr[1]+0.5*(VNz[0]+VNx[2])*POS[ii].dr[2];
	  pW[1]+=0.5*(VNx[1]+VNy[0])*POS[ii].dr[0]+            VNy[1] *POS[ii].dr[1]+0.5*(VNz[1]+VNy[2])*POS[ii].dr[2];
      pW[2]+=0.5*(VNx[2]+VNz[0])*POS[ii].dr[0]+0.5*(VNy[2]+VNz[1])*POS[ii].dr[1]+            VNz[2] *POS[ii].dr[2];
      break;
  }//switch

}//if k!=ii

}//for k


  W[0]*=dt;
  W[1]*=dt;
  W[2]*=dt;

  pW[0]*=dt;
  pW[1]*=dt;
  pW[2]*=dt;
} //velo


//--------------------------------------------------------------------------------------------------
//Влияние на ii-ый вортон
// ii - номер вортона, на который вычисляется влияние
// Результат - заполнение W, pW
void veloPoint(long double *Rtn, long double* W, bool mult_t)
{
{
  for (int i=0; i<3; i++)
  {
	W[i]=0.0;
  }//for i
}


long double VN[3];

for (int k=0; k<n; k++)
{

 //if (k!=ii)
 {
  InductPoint(Rtn,k,VN);

  W[0]+=VN[0];
  W[1]+=VN[1];
  W[2]+=VN[2];

 }//if k!=ii

}//for k

if (mult_t)
{
  W[0]*=dt;
  W[1]*=dt;
  W[2]*=dt;
}//if mult_t

} //veloPoint

//-----------------------------------------------------------------------------------------------------------

long double GetZirc()
{
	long double CNT[3]={1.0, 0.0, 0.0}; //Центр мерного кольца
	long double RR = 1.0; //Радиус мерного кольца
	int NN=360; //Дискретизация мерного кольца
	//ВНИМАНТЕ:: МЕРНОЕ КОЛЬЦО В ПЛОСКОСТИ XOY
	long double stepfi=dpi/NN;

	long double res=0.0;

	for(int i=0; i<NN; i++)
	{
	long double pnt[3];
	pnt[0]=CNT[0]+RR*cosl(i*stepfi);
	pnt[1]=CNT[1]+RR*sinl(i*stepfi);
	pnt[2]=CNT[2];

	long double W[3];
	veloPoint(pnt,W,false);

	long double tau[3];
	tau[0]=-RR*sinl(i*stepfi);
	tau[1]= RR*cosl(i*stepfi);
	tau[2]= 0.0;

	for (int j=0; j<3; j++)
	 res+=W[j]*tau[j]*stepfi;
	}//for i

return res;
}//GetZirk




//-----------------------------------------------------------------------------------------------------------


//Распределение задачи по процессам
void PreSpleat()
{
	int sum=0;
	for (int s=0; s<numprocs-1; s++)
		{
		session_start[s]=sum;
		session_length[s]=(int)(n*session_spd[s]/session_spd_sum);
		sum+=session_length[s];
		}//for i

	session_start[numprocs-1]=sum;
	session_length[numprocs-1]=n-sum;

	//printf("Y[last]: %16f\n",R[n-1][1]);
	//printf("Hello: n=%d\n",n);
}//PreSpleat




//--------------------------------------------------------------
//-------------- Ядро вычислительного конвейера, ---------------
//------------- которое запускается на всех узлах---------------
//--------------------------------------------------------------

void GetSpeeds()
{
	//Рассылка коэффициента для умножения на dt
	//MPI_Bcast(&cft,  1, MPI_LONG_DOUBLE, 0, MPI_COMM_WORLD);

	//Рассылка величины шага dt
	MPI_Bcast(&dt,   1, MPI_LONG_DOUBLE, 0, MPI_COMM_WORLD);

	//Рассылка количества вихрей в пелене n
	MPI_Bcast(&n,    1, MPI_INT,    0, MPI_COMM_WORLD);




	//Рассылка вортонов
	MPI_Bcast(POS, n, mpiVorton, 0, MPI_COMM_WORLD);

	//Рассылка распределения задачи по узлам
	MPI_Bcast(session_start,  numprocs, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(session_length, numprocs, MPI_INT, 0, MPI_COMM_WORLD);

	//Начало вычислительной процедуры
	long double startwtime_loc, endwtime_loc, time_loc;

	//Первая засечка времени
	startwtime_loc = MPI_Wtime();


	PVorton VEL_loc[max_vorton];


	//Вычисление скорости, индуцируемой остальными вихрями (идеальная жидкость)
	for (int r=0; r<session_length[myid]; r++)
		{
		long double V[3];
		long double sW[3];

		//Вычисление влияния на вихри
		velo(r+session_start[myid],V,sW);

		//velo(r+session_start[myid],V,sW,sE,sL);

			{
			for (int i=0; i<3; i++)
			{
			VEL_loc[r].pr[i]=V[i];
			VEL_loc[r].pdr[i]=sW[i];
			}//for i
			}

		//if (r==1)
	    // printf("V[%d]: %.16f, %.16f\n",r,Vxdt[r]/dt,Vydt[r]/dt);
		} //for r



	//Вторая засечка времени
	endwtime_loc = MPI_Wtime();

	//Вычисление разницы времен (в секундах)
	time_loc=endwtime_loc-startwtime_loc;

	//Вычисление производительности узла (вихрей в секунду)
	long double spd_loc=session_length[myid]/time_loc;

	//Возвращаем скорости вихрей, умноженные на шаг с нужным коэффициентом
    MPI_Gatherv(VEL_loc,session_length[myid],mpiPVorton,VEL,session_length,session_start,mpiPVorton,0,MPI_COMM_WORLD);


	//Возвращаем производительности узлов (кол-во вихрей, обрабатываемых за единицу времени)
	MPI_Gather(&spd_loc,1,MPI_LONG_DOUBLE,session_spd,1,MPI_LONG_DOUBLE,0,MPI_COMM_WORLD);

    //Считаем сумму производительностей узлов (для нормировки при расперделении
	//объёма вычислений на следующем шаге
	MPI_Reduce(&spd_loc, &session_spd_sum, 1, MPI_LONG_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	//Возвращаем (на всякий случай!) время, затраченное узлом на решение своей задачи
	MPI_Gather(&time_loc,1,MPI_LONG_DOUBLE,session_time,1,MPI_LONG_DOUBLE,0,MPI_COMM_WORLD);
}





void GetSpeedsGraph()
{
int n_C[3];  //Число отрезков (ячеек) в сетке
n_C[0]=200;
n_C[1]=1;
n_C[2]=1;


long double R_C[3]; //Центр Сетки
R_C[0]=0.0;
R_C[1]=0.0;
R_C[2]=0.0;

long double L_C[3]; //Полуразмахи сетки
L_C[0]=2.0;
L_C[1]=0.0;
L_C[2]=0.0;

long double dL_C[3];  //Шаги сетки
{
for (int i=0; i<3; i++)
 dL_C[i]=2.0*L_C[i] / n_C[i];
}

long double Rtest[3];
long double Vtest[3];
Rtest[2]=0.0;


spdfile.open("speedgrf.txt");
//spdfile << (n_C[0]+1) << " " << (n_C[1]+1) << endl;



for (int i=0; i<=n_C[0]; i++)
{
//	for (int j=0; j<=n_C[1]; j++)
	int j=0;
	{
	    Rtest[0]=R_C[0]-L_C[0]+i*dL_C[0];
		Rtest[1]=R_C[1]-L_C[1]+j*dL_C[1];
		veloPoint(Rtest,Vtest,false);
		spdfile << Rtest[0] << " "  << (Vtest[0]) << " " << (Vtest[1]) << " " << (Vtest[2]) << " " << endl;


	}//j
}//i

spdfile.close();


} //GetSpeedsGraph





void GetPressGraph()
{
int n_C[3];  //Число отрезков (ячеек) в сетке
n_C[0]=200;
n_C[1]=1;
n_C[2]=1;


long double R_C[3]; //Центр Сетки
R_C[0]=0.0;
R_C[1]=0.0;
R_C[2]=0.0;

long double L_C[3]; //Полуразмахи сетки
L_C[0]=2.0;
L_C[1]=0.0;
L_C[2]=0.0;

long double dL_C[3];  //Шаги сетки
{
for (int i=0; i<3; i++)
 dL_C[i]=2.0*L_C[i] / n_C[i];
}

long double Rtest[3];
long double Vtest[3];
Rtest[2]=0.0;

prsfile.open("pressgrf.txt");
//spdfile << (n_C[0]+1) << " " << (n_C[1]+1) << endl;


	{
	for (int www=0; www<n; www++)
	{
		POS_old[www]=POS[www];
	}
	}

	if (myid==0)
		{
		//printf("time: t=%.8f\n",t*dt);
		//printf("N: n=%d\n",n);
		PreSpleat();
		}


	GetSpeeds();



for (int i=0; i<=n_C[0]; i++)
{
	//for (int j=0; j<=n_C[1]; j++)
	int j=0;
	{
	    Rtest[0]=R_C[0]-L_C[0]+i*dL_C[0];
		Rtest[1]=R_C[1]-L_C[1]+j*dL_C[1];
		veloPoint(Rtest,Vtest,false);

		long double Q=-0.5*(Vtest[0]*Vtest[0]+Vtest[1]*Vtest[1]+Vtest[2]*Vtest[2]);

		long double ID=0.0;

		long double UU[3];
		long double VV[3];

		for (int s=0; s<n; s++)
		{
			InductPoint(Rtest,s,UU);
			for (int k=0; k<3; k++)
			 VV[k]=VEL[s].pr[k]/dt;

		    ID+=UU[0]*VV[0]+UU[1]*VV[1]+UU[2]*VV[2];

		}//s

		prsfile <<  Rtest[0] << " " << 1.0+(Q+ID) << endl;

	}//j
}//i



prsfile.close();

} //GetPressGraph




void GetSpeedsGrid()
{
int n_C[3];  //Число отрезков (ячеек) в сетке
n_C[0]=50;
n_C[1]=50;
n_C[2]=1;


long double R_C[3]; //Центр Сетки
R_C[0]=1.0;
R_C[1]=0.5;
R_C[2]=0.0;

long double L_C[3]; //Полуразмахи сетки
L_C[0]=1.0;
L_C[1]=1.0;
L_C[2]=0.0;

long double dL_C[3];  //Шаги сетки
{
for (int i=0; i<3; i++)
 dL_C[i]=2.0*L_C[i] / n_C[i];
}

long double Rtest[3];
long double Vtest[3];
Rtest[2]=0.0;

spdfile.open("speed.gv");
prsfile.open("press.gdr");
spdfile << (n_C[0]+1) << " " << (n_C[1]+1) << endl;
prsfile << (n_C[0]+1) << " " << (n_C[1]+1) << endl;




	{
	for (int www=0; www<n; www++)
	{
		POS_old[www]=POS[www];
	}
	}

	if (myid==0)
		{
		//printf("time: t=%.8f\n",t*dt);
		//printf("N: n=%d\n",n);
		PreSpleat();
		}


	GetSpeeds();



for (int i=0; i<=n_C[0]; i++)
{
	for (int j=0; j<=n_C[1]; j++)
	{
	    Rtest[0]=R_C[0]-L_C[0]+i*dL_C[0];
		Rtest[1]=R_C[1]-L_C[1]+j*dL_C[1];
		veloPoint(Rtest,Vtest,false);
		spdfile << (Vtest[0]) << " " << (Vtest[1]) << " " << (Vtest[2]) << " " << endl;


		long double Q=-0.5*(Vtest[0]*Vtest[0]+Vtest[1]*Vtest[1]+Vtest[2]*Vtest[2]);

		long double ID=0.0;

		long double UU[3];
		long double VV[3];

		for (int s=0; s<n; s++)
		{
			InductPoint(Rtest,s,UU);
			for (int k=0; k<3; k++)
			 VV[k]=VEL[s].pr[k]/dt;

		    ID+=UU[0]*VV[0]+UU[1]*VV[1]+UU[2]*VV[2];

		}//s

		prsfile << 1.0+(Q+ID) << endl;

	}//j
}//i

spdfile.close();
prsfile.close();

}//GetSpeedsGrid()















//--------------------------------------------------------------
//--------------          MAIN PROGRAM           ---------------
//--------------------------------------------------------------
int main(int argc, char* argv[])
{

//#ifdef __linux__
 //printf("LINUX ");
//#else
 //printf("NO_LINUX ");
//#endif


//{
//long double x = 1E-500L;
//printf("x: x=%E\n",x);
//}

//{
//long double x = 1E-200L;
//printf("x: x=%E\n",x);
//}


    logfile.open("LOG.txt");
	telefile.open("TEL.txt");
	timefile.open("TST.txt");






	MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    MPI_Get_processor_name(processor_name,&namelen);

    //fprintf(stderr,"Process %d on %s\n",myid, processor_name);
	fflush(stderr);



	/* подготовка вортона */
	{
 	int          len[4] = { 3, 3, 1, 1};
	MPI_Aint     pos[4] = { offsetof(Vorton,r), offsetof(Vorton,dr), offsetof(Vorton,G), sizeof(Vorton) };
	MPI_Datatype typ[4] = { MPI_LONG_DOUBLE,MPI_LONG_DOUBLE,MPI_LONG_DOUBLE,MPI_UB };

	MPI_Type_struct( 4, len, pos, typ, &mpiVorton);
	MPI_Type_commit( &mpiVorton);
	}

	{
 	int          len[3] = { 3, 3, 1};
	MPI_Aint     pos[3] = { offsetof(PVorton,pr), offsetof(PVorton,pdr), sizeof(PVorton) };
	MPI_Datatype typ[3] = { MPI_LONG_DOUBLE,MPI_LONG_DOUBLE,MPI_UB };

	MPI_Type_struct( 3, len, pos, typ, &mpiPVorton);
	MPI_Type_commit( &mpiPVorton);
	/* подготовка закончена */
	}







	if (myid == 0)
	{
	timefile << "Step" << " " //<< "SLAU_form" << " " << "SLAU_solve" << " " << "SaveToFiles" << " "
		     //<< "GetSpeeds" << " " << "Press" << " " << "Move" << " "  << "Hlop" << " "
			 << "TOTAL" << " " <<endl;


	startwtime = MPI_Wtime();
	}

	if (myid == 0)
	{

		n=0;

		ifstream pspfile;
		pspfile.open("PS.txt");

		char st[255];

		pspfile >> st;
		pspfile >> start;
		pspfile >> st;
		pspfile >> stop;
		pspfile >> st;
		pspfile >> shema;
		pspfile >> st;
		pspfile >> usertypeg;
		pspfile >> st;
		pspfile >> dt;
		pspfile >> st;
		pspfile >> eps;
		           eps2=eps*eps;
		pspfile >> st;
		pspfile >> deltacnt;
		pspfile >> st;
		pspfile >> st;

		pspfile >> nBUB;
		pspfile >> st;

		for (int ib=0; ib<nBUB; ib++)
		{

			long double cntr[3];
			pspfile >> cntr[0]; pspfile >> cntr[1]; pspfile >> cntr[2];
			pspfile >> st;


			ifstream infile;
			infile.open(st);

			pspfile >> st;

			int temp;

			infile >> nVTN; //Считывание числа вихрей в пелене


			//*
			for (int i=0; i<nVTN; i++)
				{
				infile >> temp;
				infile >> POS[n].G;

				infile >> POS[n].r[0];
				infile >> POS[n].r[1];
				infile >> POS[n].r[2];

				POS[n].r[0]+=cntr[0];
				POS[n].r[1]+=cntr[1];
				POS[n].r[2]+=cntr[2];

				infile >> POS[n].dr[0];
				infile >> POS[n].dr[1];
				infile >> POS[n].dr[2];

				n++;
				}//for i


			//*/
			infile.close();


				//Кольцо
				//infile >> temp;
				/*
				long double dfi = dpi/nVTN;

			    for (int i=0; i<nVTN; i++)
				{
				POS[n].G=1.0;

				POS[n].r[0] = 0.5*(cosl(i*dfi + 1.0)+cosl((i+1)*dfi + 1.0));
				POS[n].r[1] = 0.0;
				POS[n].r[2] = 0.5*(sinl(i*dfi + 1.0)+sinl((i+1)*dfi + 1.0));

				POS[n].r[0]+=cntr[0];
				POS[n].r[1]+=cntr[1];
				POS[n].r[2]+=cntr[2];

				POS[n].dr[0] = 0.5*(cosl(i*dfi + 1.0)-cosl((i+1)*dfi + 1.0));
				POS[n].dr[1] = 0.0;
				POS[n].dr[2] = 0.5*(sinl(i*dfi + 1.0)-sinl((i+1)*dfi + 1.0));

				n++;
				}//for i
				*/


				//Кольцо, где каждое звено состоит из нескольких отрезков
				/*
				long double dfi = dpi/nVTN;

				double base[3];
				double dbase[3];

				for (int i=0; i<nVTN; i++)
				{
				base[0] = cosl(i*dfi + 1.0);
				base[1] = 0.0;
				base[2] = sinl(i*dfi + 1.0);

				dbase[0] = -0.5*(cosl(i*dfi + 1.0)-cosl((i+1)*dfi + 1.0))/mnogVTN;
				dbase[1] = -0.0;
				dbase[2] = -0.5*(sinl(i*dfi + 1.0)-sinl((i+1)*dfi + 1.0))/mnogVTN;

				for (int q=0; q<mnogVTN; q++)
				{
					if (ib==0) POS[n].G=1.0;
					if (ib==1) POS[n].G=-1.0;

					POS[n].r[0] = base[0]+(2*q+1)*dbase[0];
					POS[n].r[1] = base[1]+(2*q+1)*dbase[1];
					POS[n].r[2] = base[2]+(2*q+1)*dbase[2];

					POS[n].r[0]+=cntr[0];
					POS[n].r[1]+=cntr[1];
					POS[n].r[2]+=cntr[2];

					POS[n].dr[0] = -dbase[0];
					POS[n].dr[1] = -dbase[1];
					POS[n].dr[2] = -dbase[2];
					n++;
				}

				}//for i
				*/

			    /*
				//Псевдоэллипс (овал)
				long double Re = 0.5;
				long double L  = 7.0;  // 7 - "длинное" кольцо,  3 - "короткое" кольцо

				int NR = 20;  //Число вортонов на полуокружности

				long double gam = 1.0;

				long double dfi = dpi/(2.0*NR);

				long double DLa = 0.5 * dpi * Re / NR;

				int NL = (int)((L - 2.0*Re)/DLa) + 1;

				long double DL = (L - 2.0*Re)/NL;

				dl_min_etalon = 0.0*(0.5*DL); //не сжиматься сильнее, чем в 8 раза
				dl_max_etalon = 2.0*(0.5*DL); //не удлиняться сильнее, чем в 2 раза

				{
				//Законцовка сверху:
				for (int i=0; i<NR; i++)
				{
				POS[n].r[0] = Re*0.5*(cosl(i*dfi)+cosl((i+1.0)*dfi));
				POS[n].r[1] = 0.0;
				POS[n].r[2] = Re*0.5*(sinl(i*dfi)+sinl((i+1.0)*dfi)) + (0.5*L - Re);

				//POS[n].r[0]+=cntr[0];
				//POS[n].r[1]+=cntr[1];
				//POS[n].r[2]+=cntr[2];

				POS[n].dr[0] = Re*0.5*(cosl(i*dfi)-cosl((i+1.0)*dfi));
				POS[n].dr[1] = 0.0;
				POS[n].dr[2] = Re*0.5*(sinl(i*dfi)-sinl((i+1.0)*dfi));

				POS[n].G=gam;
				n++;
				}//for i
				}


				{
				//Левая сторона
				for (int i=0; i<NL; i++)
				{
				POS[n].r[0] = -Re;
				POS[n].r[1] = 0.0;
				POS[n].r[2] = (0.5*L - Re) - 0.5*DL - i*DL;

				//POS[n].r[0]+=cntr[0];
				//POS[n].r[1]+=cntr[1];
				//POS[n].r[2]+=cntr[2];

				POS[n].dr[0] = 0.0;
				POS[n].dr[1] = 0.0;
				POS[n].dr[2] = 0.5*DL;

				POS[n].G=gam;
				n++;
				}//for i
				}

				{
				//Законцовка снизу:
				for (int i=0; i<NR; i++)
				{
				POS[n].r[0] = Re*0.5*(cosl(0.5*dpi+i*dfi)+cosl(0.5*dpi+(i+1)*dfi));
				POS[n].r[1] = 0.0;
				POS[n].r[2] = Re*0.5*(sinl(0.5*dpi+i*dfi)+sinl(0.5*dpi+(i+1)*dfi)) - (0.5*L - Re);

				//POS[n].r[0]+=cntr[0];
				//POS[n].r[1]+=cntr[1];
				//POS[n].r[2]+=cntr[2];

				POS[n].dr[0] = -Re*0.5*(cosl(i*dfi)-cosl((i+1)*dfi));
				POS[n].dr[1] = -0.0;
				POS[n].dr[2] = -Re*0.5*(sinl(i*dfi)-sinl((i+1)*dfi));

				POS[n].G=gam;
				n++;
				}//for i
				}


				{
				//Правая сторона
				for (int i=0; i<NL; i++)
				{
				POS[n].r[0] = Re;
				POS[n].r[1] = 0.0;
				POS[n].r[2] = -(0.5*L - Re) + 0.5*DL + i*DL;

				//POS[n].r[0]+=cntr[0];
				//POS[n].r[1]+=cntr[1];
				//POS[n].r[2]+=cntr[2];

				POS[n].dr[0] = 0.0;
				POS[n].dr[1] = 0.0;
				POS[n].dr[2] = -0.5*DL;

				POS[n].G=gam;
				n++;
				}//for i
				}
			//*/ //ОВАЛ

		}//for ib



		pspfile >> st;
		pspfile >> vtn_ndx;
		pspfile >> st;

		pspfile.close();


		for (int iprc=0; iprc<numprocs; iprc++)
			session_spd[iprc]=1.0; //На первый заход загружаем все процессы равномерно
		session_spd_sum=numprocs;
	} //if myid==0

	MPI_Bcast(&start,    1, MPI_INT,    0, MPI_COMM_WORLD);
	MPI_Bcast(&stop,     1, MPI_INT,    0, MPI_COMM_WORLD);
	MPI_Bcast(&shema,    1, MPI_INT,    0, MPI_COMM_WORLD);
	MPI_Bcast(&usertypeg,1, MPI_INT,    0, MPI_COMM_WORLD);
	MPI_Bcast(&dt,       1, MPI_LONG_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Bcast(&deltacnt, 1, MPI_INT,    0, MPI_COMM_WORLD);
	MPI_Bcast(&eps,      1, MPI_LONG_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Bcast(&eps2,     1, MPI_LONG_DOUBLE, 0, MPI_COMM_WORLD);




    logfile << "Zirc=" << GetZirc() << endl;



	//GetSpeedsGrid();

	//GetSpeedsGraph();
	//GetPressGraph();


	cnt=deltacnt-1;
	//Цикл решения задачи
	//for (int t=0; t<(int)(10.0/dt); t++)


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////


	for (int t=start; t<stop; t++)
	{

	if (myid == 0) tmm[0] = MPI_Wtime();


	{
	for (int www=0; www<n; www++)
	{
		POS_old[www]=POS[www];
	}
	}

	if (myid==0)
		{
		//printf("time: t=%.8f\n",t*dt);
		//printf("N: n=%d\n",n);
		PreSpleat();
		}

	if (myid==0)
	{
		cnt++;
		if (cnt==deltacnt)
		{
			//if (t<5000) deltacnt=500;
			//if ( (t>=5000)&&(t<6000) ) deltacnt=5;
			//if ( (t>=6000)&&(t<10000) ) deltacnt=500;
			//if ( (t>=10000)&&(t<11000) ) deltacnt=5;
			//if (t>11000) deltacnt=1000;
			save_to_file(t); //сохранение пелены на текущем шаге
			SaveKadrVtk(t);
			cnt=0;
		}
	}


	GetSpeeds();


	if (myid==0)
	{
		if (cnt==0)
		{
			save_vkadr_to_file(t); //сохранение пелены на текущем шаге
		}//if cnt
	}




	//предиктор - выполняет процесс 0
	if (myid==0)
		{
		for (int www=0; www<n; www++)
			{
			//Приращения
			for (int j=0;j<3;j++)
			{
				POS[www].r[j] +=0.5*VEL[www].pr[j];
				POS[www].dr[j]+=0.5*VEL[www].pdr[j];
				//POS[www].dr[j]+=0.0;
			}//for j


			long double mr=sqrt(L2(POS[www].dr));
			if (mr < dl_min_etalon)
			{
				for (int j=0;j<3;j++)
				{
					POS[www].dr[j]*=(dl_min_etalon/mr);
				}
			}

			if (mr > dl_max_etalon)
			{
				for (int j=0;j<3;j++)
				{
					POS[www].dr[j]*=(dl_max_etalon/mr);
				}
			}

		}//for (int www=0;




	}//if (myid==0)





	GetSpeeds();


    //корректор - выполняет процесс 0
	if (myid==0)
		{
		for (int www=0; www<n; www++)
			{
			//Приращения
			for (int j=0;j<3;j++)
			{
				POS[www].r[j] =POS_old[www].r[j] +VEL[www].pr[j];
				POS[www].dr[j]=POS_old[www].dr[j]+VEL[www].pdr[j];
				//POS[www].dr[j]=POS_old[www].dr[j];
			}//for j

			long double mr=sqrt(L2(POS[www].dr));
			if (mr < dl_min_etalon)
			{
				for (int j=0;j<3;j++)
				{
					POS[www].dr[j]*=(dl_min_etalon/mr);
				}
			}

			if (mr > dl_max_etalon)
			{
				for (int j=0;j<3;j++)
				{
					POS[www].dr[j]*=(dl_max_etalon/mr);
				}
			}

			}//for (int www=0;
		}//if (myid==0)





	if (myid == 0)
		{
		//cnt++;
		{
			//if (cnt==deltacnt)
			//{
			// save_to_file(t); //сохранение пелены на текущем шаге
			// cnt=0;
			//}



			//Средние координаты всех вихрей
			long double pos[3];
			long double Ro;

			//Первый бублик
			{
			for (int i=0; i<3; i++)
			{
				pos[i]=0.0;
			}//for i
			Ro=0.0;
				for (int s=0; s<nVTN*mnogVTN; s++)
				{
					pos[0]+=POS[s].r[0];
					pos[1]+=POS[s].r[1];
					pos[2]+=POS[s].r[2];
					Ro+=sqrtl(POS[s].r[0]*POS[s].r[0]+POS[s].r[2]*POS[s].r[2]);
				}
			for (int i2=0; i2<3; i2++)
			{
				pos[i2]/=(nVTN*mnogVTN);
			}//for i2
			Ro/=(nVTN*mnogVTN);

			logfile << t*dt << " " << pos[0] << " " << pos[1] <<" " << pos[2] << " " << Ro << " ";
			//logfile << t*dt << " " << pos[1] <<" " << Ro << " ";
			}//Первый бублик



			//Второй бублик
			{
			for (int i=0; i<3; i++)
			{
				pos[i]=0.0;
			}//for i
			Ro=0.0;
				for (int s=(nVTN*mnogVTN); s<2*(nVTN*mnogVTN); s++)
				{
					pos[0]+=POS[s].r[0];
					pos[1]+=POS[s].r[1];
					pos[2]+=POS[s].r[2];
					Ro+=sqrtl(POS[s].r[0]*POS[s].r[0]+POS[s].r[2]*POS[s].r[2]);
				}
			for (int i2=0; i2<3; i2++)
			{
				pos[i2]/=(nVTN*mnogVTN);
			}//for i2
			Ro/=(nVTN*mnogVTN);

			logfile << " " << pos[0] << " " << pos[1] <<" " << pos[2] << " " << Ro << " ";// << endl;
			//logfile << " " << pos[1] <<" " << Ro << endl;
			}//Второй бублик

			//telefile << t << " ";
			/*
			{
			long double base[2];
			long double fact[2];
			base[0]=sqrtl(POS[0].r[0]*POS[0].r[0]+POS[0].r[2]*POS[0].r[2]);
			base[1]=POS[0].r[1];
			for (int i=1; i<(nVTN*mnogVTN); i++)
			{
				fact[0]=sqrtl(POS[i].r[0]*POS[i].r[0]+POS[i].r[2]*POS[i].r[2]);
				fact[1]=POS[i].r[1];
				telefile << fact[0]-base[0] << " " << fact[1]-base[1] << " ";
			}
			}
			telefile << endl;
			*/

			/*
			int vtn_ndx=0;
			telefile << t*dt << " " << POS[vtn_ndx].r[0] <<" " << POS[vtn_ndx].r[1] << " " << POS[vtn_ndx].r[2] << " ";
			long double len=sqrtl(L2(POS[vtn_ndx].dr))*2.0;
			long double ncos[3];
			{
			for (int i=0;i<3;i++)
				ncos[i]=POS[vtn_ndx].dr[i]/len;
			}
            telefile << len << " " << ncos[0] <<" " << ncos[1] << " " << ncos[2] << endl;
			*/

			long double sumgamma[3];
			sumgamma[0]=0.0;
			sumgamma[1]=0.0;
			sumgamma[2]=0.0;

			long double angimp[3];
			angimp[0]=0.0;
			angimp[1]=0.0;
			angimp[2]=0.0;
			{
				for (int vt=0; vt<(nVTN*mnogVTN); vt++)
				{
					sumgamma[1]+=POS[vt].G     *sqrtl(L2(POS[vt].dr))     *2.0;
					sumgamma[2]+=POS[vt+(nVTN*mnogVTN)].G*sqrtl(L2(POS[vt+(nVTN*mnogVTN)].dr))*2.0;
				}

				for (int vt=0; vt<n; vt++)
				{
					long double cc[3];
					Cross(POS[vt].r,POS[vt].dr,cc);
					angimp[0]+=POS[vt].G*cc[0];
					angimp[1]+=POS[vt].G*cc[1];
					angimp[2]+=POS[vt].G*cc[2];
				}
			}
			sumgamma[0]=sumgamma[1]+sumgamma[2];
			logfile << sumgamma[0] << " " << sumgamma[1] << " " << sumgamma[2] << " ";// << endl;
			logfile << angimp[0] << " " << angimp[1] << " " << angimp[2] << " " << endl;
		}//

    if (myid == 0) tmm[1] = MPI_Wtime();


	timefile << t << " " << tmm[1]-tmm[0] //<< " " << tmm[2]-tmm[1] << " " << tmm[3]-tmm[2] << " "
			     //<< tmm[4]-tmm[3] << " " << tmm[5]-tmm[4] << " " << tmm[6]-tmm[5] << " "
				 //<< tmm[7]-tmm[6] << " " << tmm[7]-tmm[0] << " "
				   <<endl;



	} //if (myid==0)


	} //for (int t=start;



	//printf("Finish: n=%d\n",n);


	if (myid == 0)
	{
		endwtime = MPI_Wtime();
        logfile << "wall clock total time = " << endwtime-startwtime << endl;
	}


	logfile.close();
	telefile.close();
	timefile.close();

	MPI_Finalize();
	return 0;

}
