import cv2
import numpy as np
import tensorflow as tf
from config.config import config
from src.utils import preprocess_image


def predict(config):
    interpreter = tf.lite.Interpreter(model_path="weights/bestmodel.tflite")
    interpreter.allocate_tensors()

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    input_details[0]["shape"]

    image = cv2.imread(config.img_test)
    image = preprocess_image(image, config)

    interpreter.set_tensor(input_details[0]["index"], image)

    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]["index"])
    output_data = np.round(output_data, 1)

    list_ind = np.where(output_data[0] >= config.threshold)

    label_list = [config.classes[ind_label] for ind_label in list_ind[0]]

    return label_list


if __name__ == "__main__":
    print(predict(config))
