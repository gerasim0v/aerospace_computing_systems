
## Запуск сервиса

##### Запуск локально

### Настройка окружения

Сначала создать и активировать venv:

```bash
python3 -m venv venv
. venv/bin/activate
```

1. Установка зависимостей ```make install```
2. Перенести последнюю лучшую модель в папку weights (переименовать модель в "bestmodel.pt")
3. Запуск сервиса
```python3 -m uvicorn app:app --host='0.0.0.0' --port=$(APP_PORT)```,
where ```APP_PORT``` - your port
4. Запуск докера (build Dockerfile)
<br />```make build DOCKER_IMAGE=$(DOCKER_IMAGE) DOCKER_TAG=$(DOCKER_TAG)``` <br />DOCKER_IMAGE - docker image name, DOCKER_TAG - docker image tag
<br />
```
    docker run \
    -d \
    -p 0.0.0.0:5000 \
    --name=$(CONTAINER_NAME) \
    ${DOCKER_IMAGE}
```
CONTAINER_NAME - container name

## Тестирование
 ```PYTHONPATH=. pytest .```
