import typing as tp

import cv2
import numpy as np
import torch


def preprocess_image(
    image: np.ndarray, target_image_size: tp.Tuple[int, int]
) -> torch.Tensor:
    image = cv2.resize(image, target_image_size)
    image = image.astype(np.float32)

    image = np.transpose(image, (2, 0, 1))
    return torch.from_numpy(image)[None]
