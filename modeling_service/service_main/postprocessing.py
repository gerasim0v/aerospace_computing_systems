from omegaconf import OmegaConf

cfg = OmegaConf.load("config/config.yml")


def postprocess():
    classes = cfg["services"]["classifier"]["classes"]
    list_index = open("data/result.txt", "r").read().split()
    label_list = [classes[int(ind_label)] for ind_label in list_index]

    return label_list


if __name__ == "__main__":
    print(postprocess())
