### Создание diff файла
> diff файл - разница весов модели между новым и старым файлом h5.

Находясь в service_main необходимо ввести команду:
```python3
python3 utils/create_diff.py -o <путь до старой модели> -n <путь до новой модели>
```
После данной команды в папке weights создаться файл param_diff.h5

Пример команды:
```python3
python3 utils/create_diff.py -o /Users/nikitakamenev/Documents/scince/aerospace_computing_systems/modeling_service/trainingloop_tf/weights/loss_1.4210474491119385_MobileNetv2.h5 -n /Users/nikitakamenev/Documents/scince/aerospace_computing_systems/modeling_service/service_main/weights/bestmodel.h5
```

### Замена старой модели

После создания файла param_diff.h5 необходидимо слить его со старой моделью.

Находясь в service_main необходимо ввести команду:
```python3
python3 utils/swap_diff_tf.py -o <путь до старой модели> -d <путь до diff файла> -n <путь до новой модели, которая получится после сливания старой модели и diff>
```
! если не указать путь до diff файла, то он автоматически возьмется из папки weights, если он есть

Пример:
```python3
python3 utils/swap_diff_tf.py -o /Users/nikitakamenev/Documents/scince/aerospace_computing_systems/modeling_service/service_main/weights/bestmodel.h5 -d /Users/nikitakamenev/Documents/scince/aerospace_computing_systems/modeling_service/service_main/weights/param_diff.h5 -n weights/test_m
odel.h5
```

### Конвертирование новой модели в tflite формат
После создания новой модели её необходимо перевести в формат tflite.
Находясь в service_main ввести команду:
```python3
python3 utils/convert_tflite.py -m <путь до новой модели в формате h5>
```

Пример:
```python3
python3 utils/convert_tflite.py -m /Users/nikitakamenev/Documents/scince/aerospace_computing_systems/modeling_service/service_main/weights/test_model.h5
```

Новая модель в формате tflite перезапишеться в файл bestmodel.tflite
