
## Запуск сервиса

##### Запуск локально

### Настройка окружения

Сначала создать и активировать venv:

```bash
python3 -m venv venv
. venv/bin/activate
```

1. Установка зависимостей ```make install```

2. Запуск докера (build Dockerfile)
<br />```make build DOCKER_IMAGE=$(DOCKER_IMAGE) DOCKER_TAG=$(DOCKER_TAG)``` <br />DOCKER_IMAGE - docker image name, DOCKER_TAG - docker image tag
<br />

```
    docker run \
    -d \
    -p 0.0.0.0:5000 \
    --name=$(CONTAINER_NAME) \
    ${DOCKER_IMAGE}
```

CONTAINER_NAME - container name

3. Замена модели

```
docker run -v /путь_к_файлу/bestmodel.tflite:/planet_service/weights/bestmodel.tflite имя_образа
```

**Таблица определяемых классов**

|номер| название          | значение                        |
|-----| ------------------| --------------------------------|
|1    | haze              | туман                           |
|2    | primary           | равномерная растительность      |
|3    | agriculture       | коммерческое с/х                |
|4    | clear             | ясная погода                    |
|5    | water             | реки, озера                     |
|6    | habitation        | поселения                       |
|7    | road              | дорога                          |
|8    | cultivation       | фермы                           |
|9    | slash_burn        | пожар                           |
|10   | cloudy            | облако                          |
|11   | partly_cloudy     | частичная облачность            |
|12   | conventional_mine | шахта/рудник                    |
|13   | bare_ground       | голая земля (без деревьев)      |
|14   | artisinal_mine    | добыча ископаемых               |
|15   | blooming          | цветение деревьев               |
|16   | selective_logging | вырубка                         |
|17   | blow_down         | ветровой выброс                 |
