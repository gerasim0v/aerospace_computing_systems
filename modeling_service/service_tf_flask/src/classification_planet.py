import typing as tp

import numpy as np
from src.preprocess_utils import load_model, preprocess_image


class PlanetClassifier:
    def __init__(self, config: tp.Dict):
        self.config = config
        self._model_path = config["services"]["classifier"]["model_path"]
        self.threshold = config["services"]["classifier"]["threshold"]
        self.classes = config["services"]["classifier"]["classes"]

        self._model = load_model(self._model_path)

    def predict_proba(self, image: np.ndarray) -> tp.Dict[str, float]:
        return self._postprocess_predict_proba(self._predict(image))

    def _predict(self, image: np.ndarray) -> np.ndarray:
        input_details = self._model.get_input_details()
        output_details = self._model.get_output_details()

        input_shape = input_details[0]["shape"]

        batch = preprocess_image(image, input_shape)

        self._model.set_tensor(input_details[0]["index"], batch)

        self._model.invoke()

        output_data = self._model.get_tensor(output_details[0]["index"])
        output_data = np.round(output_data, 1)

        return output_data

    def _postprocess_predict(self, predict: np.ndarray) -> tp.List[str]:
        list_ind = np.where(predict[0] >= self.threshold)

        label_list = [self.classes[int(ind_label)] for ind_label in list_ind[0]]

        return label_list

    def _postprocess_predict_proba(self, predict: np.ndarray) -> tp.Dict[str, float]:
        list_ind = np.where(predict[0] >= self.threshold)

        label_list = [self.classes[int(ind_label)] for ind_label in list_ind[0]]

        return label_list
