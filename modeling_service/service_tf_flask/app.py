import os

import cv2
import numpy as np
import tensorflow as tf
from flask import Flask, jsonify, request
from omegaconf import OmegaConf
from src.classification_planet import PlanetClassifier
from waitress import serve

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
path_model = os.path.join(BASE_DIR, "weights/bestmodel.tflite")

cfg = OmegaConf.load("config/config.yml")

app = Flask(__name__)


@app.route("/predict", methods=["POST"])
def predict():
    # получаем изображение из запроса
    img_bytes = request.get_data()

    # Преобразование байтовых данных в массив numpy
    nparr = np.frombuffer(img_bytes, np.uint8)

    # Декодирование изображения из массива numpy с помощью OpenCV
    image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    # загрузка модели tflite
    interpreter = tf.lite.Interpreter(model_path=path_model)
    interpreter.allocate_tensors()

    model = PlanetClassifier(cfg)

    predict = model.predict_proba(image)
    response = {"predicted_classes": str(predict[0])}

    return jsonify(response)


if __name__ == "__main__":
    serve(app, host="0.0.0.0", port=8080)
